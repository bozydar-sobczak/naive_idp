class SamlIdpController < SamlIdp::IdpController
  layout "application"

  def idp_authenticate(email, password)
    User.new(email)
  end

  def idp_make_saml_response(user)
    encode_response user
  end

end