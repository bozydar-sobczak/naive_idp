User = Struct.new(:email) do
  def persistent
    email
  end

  def email_address
    email
  end

    def asserted_attributes
      {

        email: {
          getter: :email,
          name_format: Saml::XML::Namespaces::Formats::NameId::EMAIL_ADDRESS,
          name_id_format: Saml::XML::Namespaces::Formats::NameId::EMAIL_ADDRESS
        }
      }
    end
end