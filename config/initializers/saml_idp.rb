SamlIdp.configure do |config|
  host_name = ENV['HOST_NAME'] || 'localhost:3000'
  base = "http://#{host_name}"

  config.x509_certificate = <<-CERT
-----BEGIN CERTIFICATE-----
MIIDjzCCAnegAwIBAgIJAMWqIPsfbXs+MA0GCSqGSIb3DQEBCwUAMF4xCzAJBgNV
BAYTAk5aMRUwEwYDVQQHDAxEZWZhdWx0IENpdHkxEjAQBgNVBAoMCWVjb1BvcnRh
bDEkMCIGCSqGSIb3DQEJARYVYWRtaW5AZWNvcG9ydGFsLmNvLm56MB4XDTE3MDgw
MzAzMzYxNloXDTI3MDgwMzAzMzYxNlowXjELMAkGA1UEBhMCTloxFTATBgNVBAcM
DERlZmF1bHQgQ2l0eTESMBAGA1UECgwJZWNvUG9ydGFsMSQwIgYJKoZIhvcNAQkB
FhVhZG1pbkBlY29wb3J0YWwuY28ubnowggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAw
ggEKAoIBAQDTD6da1cfLLNEmn9dg8s7aWR3LF+g6FPWltzGsfLbtOWzKvCeti9wv
ACC5OKq2uqBvmtiHwG4uEML/C4YrzRQ1qnQBw1BkXC6seGOdhHyPLoYzpGPOazja
S7AExplSNhQZR6908Bc59MyYpFUa48UiMbzayOIR+GTYZB6dTmanac/3sn/UfLXX
wPoCv+bOjse7lghlYIIt3WQgeDSNkjRdWa0w4+qW1Vrj3U4iVkEsDWaIaARKsBR0
MP+8NcQbP0vkeSa2qUMPuXVHV+vCweDED/Qutx4PvS0ea9hp6aAv1Aa9nKCrK4TV
NIhuMGMEtUq9aSPKOp0aToRCNr+ecYgpAgMBAAGjUDBOMB0GA1UdDgQWBBTCd7oT
NU8KqmdoeD8Q1AEBUpxVszAfBgNVHSMEGDAWgBTCd7oTNU8KqmdoeD8Q1AEBUpxV
szAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQASCXaat4WerLwLModH
3lMdZ53nSdwChggJpsMFpaGZfnMVmKCOcWSXExyEchw/yKxIZeBC3T5CcnCUkeSY
vsrMIubVpM9SkY5/UIbgXgds4vrYrZBJx3jrN5kkB6Ui6UZYy/6alYdlO4rvr7ro
NLeSr8UbhvZi3FGul8Q/R8xGu3suwfHA93LosUrvnGq2GGAjo6OG9VeUTaL/5Vxa
zjw3Tklr+fnmdPQiqJaOVFWz55Z5Cps0DcfN5QSMW84yPm30lTUcHpuKqcHCbxZN
VZgGG0IMuEo99B3QIMpzkoID4VWl+Jqm3lXTMf9aALY0jmxQXRRtqdJ8hJ5LcU/q
6MuU
-----END CERTIFICATE-----
  CERT

  config.secret_key = <<-CERT
-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDTD6da1cfLLNEm
n9dg8s7aWR3LF+g6FPWltzGsfLbtOWzKvCeti9wvACC5OKq2uqBvmtiHwG4uEML/
C4YrzRQ1qnQBw1BkXC6seGOdhHyPLoYzpGPOazjaS7AExplSNhQZR6908Bc59MyY
pFUa48UiMbzayOIR+GTYZB6dTmanac/3sn/UfLXXwPoCv+bOjse7lghlYIIt3WQg
eDSNkjRdWa0w4+qW1Vrj3U4iVkEsDWaIaARKsBR0MP+8NcQbP0vkeSa2qUMPuXVH
V+vCweDED/Qutx4PvS0ea9hp6aAv1Aa9nKCrK4TVNIhuMGMEtUq9aSPKOp0aToRC
Nr+ecYgpAgMBAAECggEAfpTP0QmPxsm9MkYA/TMCXi++T5SqAVOas+V31DQD3aho
F/rabRoJVuLjvVJACFFLcUPUAy6kpdoFqMK3mENpkHoHExuZdmdW0rdtPwY7UgAc
GpLIDP9/XGSyT1rwjz7BtRuqHh70Px0TK6S7KeJa05Qy6KA7/X0UtjbtqXhg1pvn
XQhDFkGe+LUaXnq0SQCUtF4NCAvoFmC8oAP2AUtWA5INxKL4KpIxEFST75qRqMQ+
Fj/lM2HX/6Vtpmi1lQfCZ1xGUjbrEX/QuFjfjbaaMn53RFjzHf/9mPBc6EJFzDjb
IHKkFy70MzQameHAiAbCaAR25o0MMAua/Dx1xjM6EQKBgQDvmdLUDI9ECPC74TJf
dHl2qhHCNzj/5+vQgLe/cfCtaa0hbB5yYoE2CLbxbY32cXHiJ8gLEVZeGAJKA0K4
yyZLYdPxMFX6KLESAWkMxP4w+zRg0ennDMaE/maytqgf497jB8jGjWibd6R5tsd5
LXnr7f+EZEh/U79lPduilaif/QKBgQDhgcUst/+LDP6BVi3CulKbg6YEKYYcm3ty
hOuUu/TzyZeNRMhohGYzaoUe1iBjJQUuX9frL7RDLZWhBKZLCGsE++3aPU+pX23t
NudCACdaCcN8KEWCkjagCu5Ee+E3dphTGRv4A0j54cCA8cdMr53wEyZ98k4Jz+nP
bwTmCGgynQKBgGVX4tzeExrrzScqsWu+kEGo3aoQQYO8TPn797x6LRqsQBaK7LXl
BFIejYLAm/NJOKo3uV01EBrsfMthZa9T/faUgljZ4QPWgk7jWdOU3w9IsXbiUtTv
imhmUnn02Ffkwjm06S9SUJSxnlf8/5I5sgAgwpr079UrTyE7cNLsCk7hAoGBALui
AG+/AU+vliwDirvV0Fo5Ze9cpYw0Crk0w9fTtHMML5+Jtb7RO0BYxz7rJ7OW4ALA
GKZF4RVsXm3FsqAH9xQEktnd3eSt1IdmAXG3MwcFzLQHxh5oji8IKUyuTqyZbxDl
eZIZGaVhI42mnw+9qApTxNlSvYAIkSDZ3ReRszE5AoGAT7EJ6Wv62UwxooMtJKQF
jVf3d+Ve5idFqAl21ZNOqI9nS01drjNFp7U9JgLgcEV+oxgmxKjBrGBRwTXDihDD
ffk/idbS9OfXYQTQBfPkClDZqdPlA9w5FUMswZCiO57P5yEA9KCs5CAD+7OT0qPT
KHF2DVNVv0cO1hSZYIHtA1A=
-----END PRIVATE KEY-----
  CERT

  config.password           = "secret_key_password"
  config.algorithm          = :sha256
  config.organization_name  = "ecoPortal"
  config.organization_url   = "http://eco-portal-idp.com"
  config.base_saml_location = "#{base}/saml"
  config.reference_id_generator # Default: -> { UUID.generate }
  config.attribute_service_location   = "#{base}/saml/attributes"
  config.single_service_post_location = "#{base}/saml/auth"

  # Principal (e.g. User) is passed in when you `encode_response`
  #
  config.name_id.formats = { email_address: -> (principal) { principal.email } }

  #   OR
  #
  #   {
  #     "1.1" => {
  #       email_address: -> (principal) { principal.email_address },
  #     },
  #     "2.0" => {
  #       transient: -> (principal) { principal.email_address },
  #       persistent: -> (p) { p.id },
  #     },
  #   }

  # If Principal responds to a method called `asserted_attributes`
  # the return value of that method will be used in lieu of the
  # attributes defined here in the global space. This allows for
  # per-user attribute definitions.
  #
  ## EXAMPLE **
  # class User
  #   def asserted_attributes
  #     {
  #       phone: { getter: :phone },
  #       email: {
  #         getter: :email,
  #         name_format: Saml::XML::Namespaces::Formats::NameId::EMAIL_ADDRESS,
  #         name_id_format: Saml::XML::Namespaces::Formats::NameId::EMAIL_ADDRESS
  #       }
  #     }
  #   end
  # end
  #
  # If you have a method called `asserted_attributes` in your Principal class,
  # there is no need to define it here in the config.

  # config.attributes # =>
  #   {
  #     <friendly_name> => {                                                  # required (ex "eduPersonAffiliation")
  #       "name" => <attrname>                                                # required (ex "urn:oid:1.3.6.1.4.1.5923.1.1.1.1")
  #       "name_format" => "urn:oasis:names:tc:SAML:2.0:attrname-format:uri", # not required
  #       "getter" => ->(principal) {                                         # not required
  #         principal.get_eduPersonAffiliation                                # If no "getter" defined, will try
  #       }                                                                   # `principal.eduPersonAffiliation`, or no values will
  #    }                                                                      # be output
  #
  ## EXAMPLE ##
  # config.attributes = {
  #   GivenName: {
  #     getter: :first_name,
  #   },
  #   SurName: {
  #     getter: :last_name,
  #   },
  # }
  ## EXAMPLE ##

  # config.technical_contact.company = "Example"
  # config.technical_contact.given_name = "Jonny"
  # config.technical_contact.sur_name = "Support"
  # config.technical_contact.telephone = "55555555555"
  # config.technical_contact.email_address = "example@example.com"

  service_providers = {
    "some-issuer-url.com/saml" => {
      fingerprint:  "4C:E8:F8:28:1D:8A:42:FB:8F:D9:8F:8A:D3:41:CC:F6:8F:AB:EB:DE",
      metadata_url: "#{base}/saml/metadata"
    },
  }

  # `identifier` is the entity_id or issuer of the Service Provider,
  # settings is an IncomingMetadata object which has a to_h method that needs to be persisted
  config.service_provider.metadata_persister = ->(identifier, settings) {
    fname = identifier.to_s.gsub(/\/|:/, "_")
    `mkdir -p #{Rails.root.join("cache/saml/metadata")}`
    File.open Rails.root.join("cache/saml/metadata/#{fname}"), "r+b" do |f|
      Marshal.dump settings.to_h, f
    end
  }

  # `identifier` is the entity_id or issuer of the Service Provider,
  # `service_provider` is a ServiceProvider object. Based on the `identifier` or the
  # `service_provider` you should return the settings.to_h from above
  config.service_provider.persisted_metadata_getter = ->(identifier, service_provider) {
    fname = identifier.to_s.gsub(/\/|:/, "_")
    `mkdir -p #{Rails.root.join("cache/saml/metadata")}`
    full_filename = Rails.root.join("cache/saml/metadata/#{fname}")
    if File.file?(full_filename)
      File.open full_filename, "rb" do |f|
        Marshal.load f
      end
    end
  }

  # Find ServiceProvider metadata_url and fingerprint based on our settings
  config.service_provider.finder = ->(issuer_or_entity_id) do
    service_providers[issuer_or_entity_id]
  end
end